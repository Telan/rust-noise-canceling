## Credits
- Karam, M. , Khazaal, H. , Aglan, H. and Cole, C. (2014) Noise Removal in Speech Processing Using Spectral Subtraction. Journal of Signal and Information Processing, 5, 32-41. doi: 10.4236/jsip.2014.52006.

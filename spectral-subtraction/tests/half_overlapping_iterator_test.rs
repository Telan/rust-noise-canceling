mod windowing_iterator_tests {
    mod constructor {
        use spectral_subtraction::half_overlapping::HalfOverlappingBufferIterator;

        #[test]
        #[should_panic(expected = "window should not be zero")]
        fn panic_given_window_is_zero() {
            HalfOverlappingBufferIterator::new(0, 0, vec![1, 2, 3].into_iter());
        }

        #[test]
        #[should_panic(expected = "step should not be zero")]
        fn panic_given_step_is_zero() {
            HalfOverlappingBufferIterator::new(10, 0, vec![1, 2, 3].into_iter());
        }

        #[test]
        #[should_panic(expected = "step should be at least half the window")]
        fn panic_given_step_is_less_than_half_the_window() {
            HalfOverlappingBufferIterator::new(10, 4, vec![1, 2, 3].into_iter());
        }

        #[test]
        #[should_panic(expected = "step should not be at greater than window")]
        fn panic_given_step_is_greater_than_window() {
            HalfOverlappingBufferIterator::new(10, 11, vec![1, 2, 3].into_iter());
        }

        #[test]
        fn not_panic_given_correct_arguments() {
            HalfOverlappingBufferIterator::new(10, 5, vec![1, 2, 3].into_iter());
            HalfOverlappingBufferIterator::new(10, 6, vec![1, 2, 3].into_iter());
            HalfOverlappingBufferIterator::new(10, 7, vec![1, 2, 3].into_iter());
            HalfOverlappingBufferIterator::new(10, 8, vec![1, 2, 3].into_iter());
            HalfOverlappingBufferIterator::new(10, 9, vec![1, 2, 3].into_iter());
            HalfOverlappingBufferIterator::new(10, 10, vec![1, 2, 3].into_iter());
        }
    }

    mod iterator {
        use spectral_subtraction::half_overlapping::HalfOverlappingBufferIterator;

        #[test]
        fn behave_like_iterator_given_step_and_window_sizes_are_equal() {
            let expected_elements = vec![1, 2, 3, 4, 5, 6, 7];
            let source = expected_elements.clone().into_iter();
            let iterator = HalfOverlappingBufferIterator::new(4, 4, source);

            let actual_elements: Vec<_> = iterator.collect();

            assert_eq!(actual_elements, expected_elements)
        }

        #[test]
        fn overlap_last_element() {
            let expected_elements = vec![1, 2, 3, 4, 4, 5, 6, 7, 7, 1, 2, 3, 3, 4, 5, 6, 6, 7, 1, 2, 2, 3, 4, 5, 5, 6, 7, 8, 8];
            let source = vec![1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 8].into_iter();
            let iterator = HalfOverlappingBufferIterator::new(4, 3, source);

            let actual_elements: Vec<_> = iterator.collect();

            assert_eq!(actual_elements, expected_elements)
        }

        #[test]
        fn overlap_half_elements() {
            let source = vec![1, 2, 3, 4, 5, 6, 7, 8].into_iter();
            let expected_elements = vec![1, 2, 3, 4, 3, 4, 5, 6, 5, 6, 7, 8, 7, 8];
            let iterator = HalfOverlappingBufferIterator::new(4, 2, source);

            let actual_elements: Vec<_> = iterator.collect();

            assert_eq!(actual_elements, expected_elements)
        }

    }
}

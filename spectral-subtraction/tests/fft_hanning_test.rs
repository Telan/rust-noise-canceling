mod fft_hanning_test {
    use std::fs::DirBuilder;

    use const_format::concatcp;
    use hound::{WavReader, WavSpec, WavWriter};
    use plotlib::page::Page;
    use plotlib::repr::Plot;
    use plotlib::style::LineStyle;
    use plotlib::view::ContinuousView;
    use rustfft::num_traits::ToPrimitive;

    use spectral_subtraction::fft_hanning;
    use spectral_subtraction::fft_hanning::Sample;

    const TEST_OUTPUT_DIR: &str = "../target/tests/fft_hanning_test";
    const TEST_RESOURCES_DIR: &str = "test-resources/";
    //noinspection RsTypeCheck
    const NOISE_FILE: &str = concatcp!(TEST_RESOURCES_DIR, "noise.wav");
    //noinspection RsTypeCheck
    const SPEECH_FILE: &str = concatcp!(TEST_RESOURCES_DIR, "speech.wav");
    //noinspection RsTypeCheck
    const SPEECH_WITH_NOISE_FILE: &str = concatcp!(TEST_RESOURCES_DIR, "speech-with-noise.wav");

    const NOISE_SAMPLES_TO_TAKE: usize = 256 * 256;

    #[test]
    fn write_valid_file_manual_test() {
        prepare_output_directory();
        let (noise_spec, noise_samples) =
            spec_and_samples_of(NOISE_FILE);
        let (speech_with_noise_spec, speech_with_noise_samples) =
            spec_and_samples_of(SPEECH_WITH_NOISE_FILE);
        assert_eq!(noise_spec, speech_with_noise_spec);
        let output_spec = speech_with_noise_spec;
        
        let processed_speech_samples =
            fft_hanning::subtract(noise_samples.take(NOISE_SAMPLES_TO_TAKE), speech_with_noise_samples);

        write_to((output_spec, processed_speech_samples), "speech-processed.wav");
    }

    #[test]
    #[ignore]
    fn cut_audio() {
        prepare_output_directory();
        let (spec, samples) =
            spec_and_samples_of(SPEECH_WITH_NOISE_FILE);
        let seconds = 1.6f32;
        let samples_to_take = (spec.sample_rate as f32 * seconds).to_usize().unwrap();
        let output_spec = spec;
        let output_samples = samples.take(samples_to_take);

        write_to((output_spec, output_samples), "speech-with-noise.wav");
    }

    #[test]
    fn plot_all() {
        prepare_output_directory();
        let (_, speech_samples) = spec_and_samples_of(SPEECH_FILE);
        plot(speech_samples, "speech");
        let (_, noise_samples) = spec_and_samples_of(NOISE_FILE);
        plot(noise_samples, "noise");
        let (_, speech_with_noise_samples) = spec_and_samples_of(SPEECH_WITH_NOISE_FILE);
        plot(speech_with_noise_samples, "speech_with_noise");

        let (_, noise_samples) = spec_and_samples_of(NOISE_FILE);
        let (_, speech_with_noise_samples) = spec_and_samples_of(SPEECH_WITH_NOISE_FILE);

        let processed_speech_samples =
            fft_hanning::subtract(noise_samples, speech_with_noise_samples);
        plot(processed_speech_samples, "processed_speech");
    }

    fn plot(samples: impl Iterator<Item=Sample>, output_file_name: &str) {
        let data_vec = samples.enumerate().map(|(index, sample)| (index as f64, sample as f64)).collect();
        let s1: Plot = Plot::new(data_vec).line_style(
            LineStyle::new()
                .width(1f32)
                .colour("#DD3355"),
        );

        let v = ContinuousView::new()
            .add(s1);

        Page::single(&v).save(format!("{}/{}.svg", TEST_OUTPUT_DIR, output_file_name)).unwrap();
    }

    #[test]
    fn improve_speech_to_noise_ration() {
        prepare_output_directory();
        let (noise_spec, noise_samples) =
            spec_and_samples_of(NOISE_FILE);
        let (speech_spec, reference_speech_samples) =
            spec_and_samples_of(SPEECH_FILE);
        let (speech_with_noise_spec, speech_with_noise_samples) =
            spec_and_samples_of(SPEECH_WITH_NOISE_FILE);
        assert_match(noise_spec, speech_spec, speech_with_noise_spec);
        let (_, reference_noise_samples) =
            spec_and_samples_of(NOISE_FILE);
        let (_, reference_speech_with_noise) =
            spec_and_samples_of(SPEECH_WITH_NOISE_FILE);

        let processed_speech_samples =
            fft_hanning::subtract(noise_samples.take(NOISE_SAMPLES_TO_TAKE), speech_with_noise_samples);

        let reference_noise_rms = root_mean_square(reference_noise_samples.take(NOISE_SAMPLES_TO_TAKE));
        let reference_speech_with_noise_rms = root_mean_square(reference_speech_with_noise);
        let reference_speech_rms = root_mean_square(reference_speech_samples);
        let processed_speech_rms = root_mean_square(processed_speech_samples);

        let reference_speech_to_noise_ratio = (reference_speech_rms / reference_noise_rms).log(20f64);
        let processed_speech_to_noise_ratio = (processed_speech_rms / reference_noise_rms).log(20f64);
        let speech_with_noise_to_noise_ratio = (reference_speech_with_noise_rms / reference_noise_rms).log(20f64);

        println!("Speech            : {} dB", reference_speech_to_noise_ratio);
        println!("Speech with noise : {} dB", speech_with_noise_to_noise_ratio);
        println!("Processed         : {} dB", processed_speech_to_noise_ratio);
    }

    fn root_mean_square(samples: impl Iterator<Item=Sample>) -> f64 {
        let mut average = 0f64;
        for (index, sample) in samples.enumerate() {
            average += ((sample as f64).powi(2) - average) / (index + 1) as f64;
        }

        f64::sqrt(average)
    }

    fn assert_match(noise_spec: WavSpec, speech_spec: WavSpec, speech_with_noise_spec: WavSpec) {
        assert_eq!(noise_spec, speech_with_noise_spec);
        assert_eq!(speech_spec, speech_with_noise_spec);
    }


    fn prepare_output_directory() {
        DirBuilder::new()
            .recursive(true)
            .create(TEST_OUTPUT_DIR)
            .expect("Create test output directory");
    }

    fn spec_and_samples_of(wav_file: &str) -> (WavSpec, impl Iterator<Item=Sample>) {
        let reader = WavReader::open(wav_file)
            .expect(format!("Readable file at: {}", wav_file).as_str());

        let spec = reader.spec().clone();

        let samples = reader
            .into_samples::<Sample>()
            .map(|sample| sample.expect("Readable sample"));

        (spec, samples)
    }

    fn write_to(
        (spec, processed_speech_samples): (WavSpec, impl Iterator<Item=Sample>),
        filename: &str,
    ) {
        let mut writer =
            WavWriter::create(format!("{}/{}", TEST_OUTPUT_DIR, filename), spec)
                .expect("Writable output file");

        processed_speech_samples
            .for_each(|sample| writer.write_sample(sample).expect("Sample written"));
    }
}

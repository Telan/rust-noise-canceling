use queues::{Buffer, IsQueue};
use crate::fft_hanning::{FrequencyType, Sample};

pub struct OverlappingAddIterator<SourceIteratorT> where
    SourceIteratorT: Iterator<Item=FrequencyType> {
    window: usize,
    step: usize,
    source: SourceIteratorT,
    buffer: Buffer<FrequencyType>,
    window_index: usize,
}

impl<SourceIteratorT> OverlappingAddIterator<SourceIteratorT> where
    SourceIteratorT: Iterator<Item=FrequencyType> {
    pub fn new(window: usize, step: usize, source: SourceIteratorT) -> OverlappingAddIterator<SourceIteratorT> {
        if window == 0 {
            panic!("window should not be zero");
        }
        if step == 0 {
            panic!("step should not be zero");
        }
        if step * 2 < window {
            panic!("step should be at least half the window");
        }
        if step > window {
            panic!("step should not be at greater than window");
        }
        OverlappingAddIterator {
            window,
            step,
            source,
            buffer: Buffer::new(window - step),
            window_index: 0,
        }
    }
}

impl<SourceIteratorT> Iterator for OverlappingAddIterator<SourceIteratorT> where
    SourceIteratorT: Iterator<Item=FrequencyType> {
    type Item = Sample;

    fn next(&mut self) -> Option<Self::Item> {
        let window_index = self.window_index;
        self.window_index = (self.window_index + 1) % self.window;

        if window_index >= self.step {
            self.window_index = 1;
            for _ in 0..self.buffer.capacity() {
                if let Some(value) = self.source.next() {
                    self.buffer
                        .add(value)
                        .expect("Available space in buffer");
                } else {
                    return None;
                }
            }
        }

        let next = self.source.next();

        let next_value = if let Ok(first_value) = self.buffer.remove() {
            next.map(|second_value| second_value + first_value)
        } else {
            next 
        };
        
        next_value.map(|value| value)
    }
}

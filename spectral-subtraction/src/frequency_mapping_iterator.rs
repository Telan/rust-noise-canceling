use rustfft::num_complex::Complex;
use rustfft::{FftPlanner, Fft};
use rustfft::num_traits::Zero;
use std::sync::Arc;
use crate::fft_hanning::{Spectrogram, FrequencyType, SPECTROGRAM_SIZE};

pub struct FrequencyMappingIterator<SampleIteratorT, FrequencyWindowMutatorT> where
    SampleIteratorT: Iterator<Item=FrequencyType>,
    FrequencyWindowMutatorT: FnMut(&mut Spectrogram) -> ()
{
    input_sample_iterator: SampleIteratorT,
    fft: Arc<dyn Fft<FrequencyType>>,
    mutate_frequency_window: FrequencyWindowMutatorT,
    fft_inverse: Arc<dyn Fft<FrequencyType>>,
    input_buffer: Spectrogram,
    output_sample_iterator: Option<std::vec::IntoIter<FrequencyType>>,
}

impl<SampleIteratorT, FrequencyWindowMutatorT> FrequencyMappingIterator<SampleIteratorT, FrequencyWindowMutatorT> where
    SampleIteratorT: Iterator<Item=FrequencyType>,
    FrequencyWindowMutatorT: FnMut(&mut Spectrogram) -> ()
{
    pub fn new(
        input_sample_iterator: SampleIteratorT,
        mutate_frequency_window: FrequencyWindowMutatorT,
        planner: &mut FftPlanner<FrequencyType>,
    ) -> FrequencyMappingIterator<SampleIteratorT,
        FrequencyWindowMutatorT> {
        FrequencyMappingIterator {
            input_sample_iterator,
            fft: planner.plan_fft_forward(SPECTROGRAM_SIZE),
            mutate_frequency_window,
            fft_inverse: planner.plan_fft_inverse(SPECTROGRAM_SIZE),
            input_buffer: [Complex::<FrequencyType>::zero(); SPECTROGRAM_SIZE],
            output_sample_iterator: None,
        }
    }
}

impl<SampleIteratorT, FrequencyWindowMutatorT> Iterator for FrequencyMappingIterator<SampleIteratorT, FrequencyWindowMutatorT> where
    SampleIteratorT: Iterator<Item=FrequencyType>,
    FrequencyWindowMutatorT: FnMut(&mut Spectrogram) -> ()
{
    type Item = FrequencyType;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(Some(sample)) = self.output_sample_iterator.as_mut().map(|iterator| iterator.next()) {
            return Some(sample);
        }

        if let Err(_) = self.fill_input_buffer() {
            return None;
        }

        self.fft.process(&mut self.input_buffer);
        (self.mutate_frequency_window)(&mut self.input_buffer);
        self.fft_inverse.process(&mut self.input_buffer);

        let mut new_output_iterator: std::vec::IntoIter<FrequencyType> =
            self.input_buffer
                .iter()
                .map(|complex_sample| complex_sample.re / SPECTROGRAM_SIZE as FrequencyType)
                .collect::<Vec<FrequencyType>>()
                .into_iter();

        let next_sample = new_output_iterator.next().expect("Next output sample");

        self.output_sample_iterator = Some(new_output_iterator);

        Some(next_sample)
    }
}

impl<SampleIteratorT, FrequencyWindowMutatorT> FrequencyMappingIterator<SampleIteratorT, FrequencyWindowMutatorT> where
    SampleIteratorT: Iterator<Item=FrequencyType>,
    FrequencyWindowMutatorT: FnMut(&mut Spectrogram) -> ()
{
    fn fill_input_buffer(&mut self) -> Result<(), ()> {
        for buffer_element in self.input_buffer.iter_mut() {
            if let Some(input_sample) = self.input_sample_iterator.next()
            {
                *buffer_element = Complex::from(input_sample);
            } else {
                return Err(());
            }
        }
        Ok(())
    }
}

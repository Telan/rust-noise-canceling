use std::f32::consts::PI;

use queues::{Buffer, IsQueue};
use crate::fft_hanning::FrequencyType;

pub struct HalfOverlappingBufferIterator<SourceIteratorT> where
    SourceIteratorT: Iterator<Item=FrequencyType> {
    window: usize,
    step: usize,
    source: SourceIteratorT,
    buffer: Buffer<FrequencyType>,
    window_index: usize,
}

impl<SourceIteratorT> HalfOverlappingBufferIterator<SourceIteratorT> where
    SourceIteratorT: Iterator<Item=FrequencyType> {
    pub fn new(window: usize, step: usize, source: SourceIteratorT) -> HalfOverlappingBufferIterator<SourceIteratorT> {
        if window == 0 {
            panic!("window should not be zero");
        }
        if step == 0 {
            panic!("step should not be zero");
        }
        if step * 2 < window {
            panic!("step should be at least half the window");
        }
        if step > window {
            panic!("step should not be at greater than window");
        }
        HalfOverlappingBufferIterator {
            window,
            step,
            source,
            buffer: Buffer::new(window - step),
            window_index: 0,
        }
    }
}

impl<SourceIteratorT> Iterator for HalfOverlappingBufferIterator<SourceIteratorT> where
    SourceIteratorT: Iterator<Item=FrequencyType> {
    type Item = FrequencyType;

    fn next(&mut self) -> Option<Self::Item> {
        let window_index = self.window_index;
        self.window_index = (self.window_index + 1) % self.window;


        let value = if window_index >= self.step {
            if let Some(value) = self.source.next() {
                self.buffer
                    .add(value.clone())
                    .expect("Available space in buffer");
                Some(value)
            } else {
                None
            }
        } else if window_index < self.buffer.capacity() {
            if let Ok(value) = self.buffer.remove() {
                Some(value)
            } else {
                self.source.next()
            }
        } else {
            self.source.next()
        };
        
        let hanning_window_coefficient = 0.5f32 * (1f32 + ((2f32 * PI) / self.window as f32) * window_index as f32);
        value.map(|some| some*hanning_window_coefficient)
    }
}

use rustfft::{Fft, FftPlanner};
use std::sync::Arc;
use rustfft::num_complex::Complex;
use rustfft::num_traits::Zero;
use crate::fft_hanning::{SpectrogramMagnitude, FrequencyType, Spectrogram, Sample, SPECTROGRAM_SIZE, MagnitudeType};

pub struct AverageSpectrogramMagnitudeAccumulator<SampleIteratorT> where
    SampleIteratorT: Iterator<Item=Sample> {
    input_sample_iterator: SampleIteratorT,
    input_buffer: Spectrogram,
    fft: Arc<dyn Fft<FrequencyType>>,
    output: SpectrogramMagnitude,
    spectrogram_count: usize,
}

impl<SampleIteratorT> AverageSpectrogramMagnitudeAccumulator<SampleIteratorT> where
    SampleIteratorT: Iterator<Item=Sample>
{
    pub fn new(
        input_sample_iterator: SampleIteratorT,
        planner: &mut FftPlanner<FrequencyType>,
    ) -> AverageSpectrogramMagnitudeAccumulator<SampleIteratorT> {
        AverageSpectrogramMagnitudeAccumulator {
            input_sample_iterator,
            input_buffer: [Complex::<FrequencyType>::zero(); SPECTROGRAM_SIZE],
            fft: planner.plan_fft_forward(SPECTROGRAM_SIZE),
            output: [MagnitudeType::zero(); SPECTROGRAM_SIZE],
            spectrogram_count: 0,
        }
    }

    pub fn accumulate(mut self) -> SpectrogramMagnitude {
        loop {
            if let Err(_) = self.fill_input_buffer() {
                return self.output;
            }

            self.fft.process(&mut self.input_buffer);

            self.update_output_average();
        }
    }

    // to prevent overflow we are doing in-place average like in https://www.geeksforgeeks.org/program-for-average-of-an-array-without-running-into-overflow/
    fn update_output_average(&mut self) {
        for (average_magnitude, input_frequency) in self.output.iter_mut().zip(self.input_buffer.iter()) {
            *average_magnitude += (input_frequency.norm() - *average_magnitude) / (self.spectrogram_count + 1) as FrequencyType;
        }
        self.spectrogram_count += 1;
    }

    fn fill_input_buffer(&mut self) -> Result<(), ()> {
        fill(&mut self.input_buffer, &mut self.input_sample_iterator)
    }

    
}

fn fill(buffer: &mut Spectrogram, source: &mut impl Iterator<Item=Sample>) -> Result<(), ()> {
    for buffer_element in buffer.iter_mut() {
        if let Some(input_sample) = source.next()
        {
            *buffer_element = Complex::from(input_sample as FrequencyType);
        } else {
            return Err(());
        }
    }
    Ok(())
}

pub mod half_overlapping;
pub mod overlaping_add;
pub mod frequency_mapping_iterator;
pub mod average_spectrogram_magnitude_accumulator;

pub mod fft_hanning {
    use rustfft::{FftPlanner, num_complex::Complex};
    use rustfft::num_traits::Zero;

    use crate::average_spectrogram_magnitude_accumulator::AverageSpectrogramMagnitudeAccumulator;
    use crate::frequency_mapping_iterator::FrequencyMappingIterator;
    use crate::half_overlapping::HalfOverlappingBufferIterator;
    use crate::overlaping_add::OverlappingAddIterator;

    pub type Sample = f32;

    pub const SPECTROGRAM_SIZE: usize = 256;
    pub const WINDOW_SIZE: usize = 256;
    pub const WINDOW_OVERLAP: usize = 64;
    pub const WINDOW_STEP: usize = WINDOW_SIZE-WINDOW_OVERLAP;

    pub type FrequencyType = f32;
    pub type Frequency = Complex<FrequencyType>;
    pub type Spectrogram = [Frequency; SPECTROGRAM_SIZE];

    pub type MagnitudeType = f32;
    pub type SpectrogramMagnitude = [MagnitudeType; SPECTROGRAM_SIZE];

    pub fn subtract(noise: impl Iterator<Item=Sample>, sound: impl Iterator<Item=Sample>) -> impl Iterator<Item=Sample> {
        let mut planner = FftPlanner::<FrequencyType>::new();
        let accumulator = AverageSpectrogramMagnitudeAccumulator::new(noise, &mut planner);
        let noise_average_spectrogram_magnitudes = accumulator.accumulate();

        let subtract_noise = move |spectrogram: &mut Spectrogram| {
            for (sound_frequency, noise_average_magnitude) in spectrogram.iter_mut().zip(noise_average_spectrogram_magnitudes.iter()) {
                let magnitude = sound_frequency.norm();
            
                let cleaned_magnitude = if magnitude <= *noise_average_magnitude {
                    MagnitudeType::zero()
                } else {
                    magnitude - (*noise_average_magnitude)
                };
            
                *sound_frequency = Frequency::from_polar(cleaned_magnitude, sound_frequency.arg());
            }
        };

        wrap_in_overlapping_add(
            FrequencyMappingIterator::new(
                wrap_in_overlapping_windows(sound),
                subtract_noise,
                &mut planner,
            )
        )
    }

    fn wrap_in_overlapping_windows(iterator: impl Iterator<Item=Sample>) -> impl Iterator<Item=FrequencyType> {
        HalfOverlappingBufferIterator::new(WINDOW_SIZE, WINDOW_STEP, iterator)
    }


    fn wrap_in_overlapping_add(iterator: impl Iterator<Item=FrequencyType>) -> impl Iterator<Item=Sample> {
        OverlappingAddIterator::new(WINDOW_SIZE, WINDOW_STEP, iterator)
    }
}
